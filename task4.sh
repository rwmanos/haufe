#!/bin/bash
#A bash script that correlates (via PID) all running processes with open network ports (output should print the binary path and the port number, one per line)

LISTENING=`lsof -i | grep "(LISTEN)" | awk '{print $2}'`

echo "PID   PATH"

for LPID in $LISTENING
do
    LPIDPATH=`ps $LPID | awk '{print $5}' | tail -1`
    echo $LPID $LPIDPATH
done
