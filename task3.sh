#!/bin/bash
# A bash script that lists all users on the system (output should print only the usernames, one per line)
cat /etc/passwd | sed "s/:.*$//" | sort | uniq
